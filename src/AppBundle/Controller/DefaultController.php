<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        
        return $this->render('base3.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            ]);
    }

    /**
     * @Route("/language", name="language")
     */
    public function languageAction(Request $request)
    {
        
        return $this->render('langues.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            ]);
    }

       /**
     * @Route("/for", name="for")
     */
    public function forAction(Request $request)
    {
        
        return $this->render('for.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            ]);
    }

      /**
     * @Route("/method", name="method")
     */
    public function methodAction(Request $request)
    {
        
        return $this->render('notremethode.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            ]);
    }
    
  /**
     * @Route("/quality", name="quality")
     */
    public function qualityAction(Request $request)
    {
        
        return $this->render('methode_qualite.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            ]);
    }

    /**
     * @Route("/inscription", name="inscription")
     */
    public function inscriptionAction(Request $request)
    {
        
        return $this->render('comment_inscrire.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            ]);
    }

    /**
     * @Route("/comprendre", name="comprendre")
     */
    public function comprendreAction(Request $request)
    {
        
        return $this->render('comment_comprendre.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            ]);
    }

        /**
     * @Route("/comprendre", name="comprendre")
     */
    public function priceAction(Request $request)
    {
        
        return $this->render('nos_prix.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            ]);
    }

            /**
     * @Route("/partners", name="partners")
     */
    public function partnersAction(Request $request)
    {
        
        return $this->render('partenaires.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            ]);
    }

            /**
     * @Route("/contact", name="contact")
     */
    public function contactAction(Request $request)
    {
        
        return $this->render('contact.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            ]);
    }

            /**
     * @Route("/galerie", name="galerie")
     */
    public function galerieAction(Request $request)
    {
        
        return $this->render('galerie.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            ]);
    }
}