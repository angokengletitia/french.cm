function buildApercue() {
    var titre = $('#scribobundle_commande_subject').val(),
            categorie = $('#scribobundle_commande_subjectType').val(),
            pronomPersonnel = $('#scribobundle_commande_preferredVoice').val(),
            motCle = $('#scribobundle_commande_keywords').val(),
            laCible = $('#scribobundle_commande_audience').val(),
            lienDescriptif = $('#scribobundle_commande_link').val(),
            image = $('#scribobundle_commande_freeImage').val(),
            langue = $('#scribobundle_commande_language').val(),
            petiteDescription = $('#scribobundle_commande_description').val(),
            fichier = $('#scribobundle_commande_file').val(),
            total = $('.affMontantCache').text();

    if (titre == "") {
        alert("Votre formulaire de commande n'a pas été bien rempli.\n Le Titre du sujet doit être renseigné.");
    }

    $('.sujet').text(titre);
    $('.typeSujet').text(categorie);
    $('.typeEcri').text(pronomPersonnel);
    $('.motCle').text(motCle);
    $('.cible').text(laCible);
    $('.lien').text(lienDescriptif);
    $('.image').text(image);
    $('.langue').text(langue);
    $('.description').text(petiteDescription);
    $('.fichier').text(fichier);
    $('.total').text(total);

}

function submitForm() {
    document.getElementById('form_commande').submit();
}
