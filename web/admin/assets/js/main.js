// @author : Hydil Aicard Sokeing for GreenSoft-Group Technologies
// @email: shydilaicard@gmail.com
// @creation_date : 27/01/2020

$(function() {

    var APP_ROOT;

    (window.location.pathname.match(/(.*?)web/i)) ? (APP_ROOT = window.location.pathname.match(/(.*?)web/i)[1]) : (APP_ROOT = "");
    (APP_ROOT) ? (APP_ROOT += "web") : (APP_ROOT);

    var table = document.querySelector('.datatable');
    if (table){
        head.load([
            '/admin/assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js',
            '/admin/assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js',
            '/admin/assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js',
            '/admin/assets/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js',
            '/admin/assets/plugins/bootstrap-datatable/js/jszip.min.js',
            '/admin/assets/plugins/bootstrap-datatable/js/pdfmake.min.js',
            '/admin/assets/plugins/bootstrap-datatable/js/vfs_fonts.js',
            '/admin/assets/plugins/bootstrap-datatable/js/buttons.html5.min.js',
            '/admin/assets/plugins/bootstrap-datatable/js/buttons.print.min.js',
            '/admin/assets/plugins/bootstrap-datatable/js/buttons.colVis.min.js'
        ], function () {
            var table = $('#example').DataTable( {
                'language': {
                    'sProcessing': 'Traitement en cours...',
                    'sSearch': 'Rechercher&nbsp;:',
                    'sLengthMenu': 'Afficher _MENU_ &eacute;l&eacute;ments',
                    'sInfo': 'Affichage de l\'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments',
                    'sInfoEmpty': 'Affichage de l\'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment',
                    'sInfoFiltered': '(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)',
                    'sInfoPostFix': '',
                    'sLoadingRecords': 'Chargement en cours...',
                    'sZeroRecords': 'Aucun &eacute;l&eacute;ment &agrave; afficher',
                    'sEmptyTable': 'Aucune donn&eacute;e disponible dans le tableau',
                    'oPaginate': {
                        'sFirst': 'Premier',
                        'sPrevious': 'Pr&eacute;c&eacute;dent',
                        'sNext': 'Suivant',
                        'sLast': 'Dernier'
                    },
                    'oAria': {
                        'sSortAscending': ': activer pour trier la colonne par ordre croissant',
                        'sSortDescending': ': activer pour trier la colonne par ordre d&eacute;croissant'
                    }
                },
                lengthChange: false,
                "aaSorting": [],
//                buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
                buttons: [ 'excel', 'pdf', 'print']
            } );
            table.buttons().container().appendTo( '#example_wrapper .col-md-6:eq(0)');

        });
    }


    var websitePhone = document.querySelector('.websitePhone');
    if (websitePhone) {
        head.load([
            '/admin/assets/js/intlTelInput.js'
        ], function () {
            var office = document.querySelector("#website_officePhoneNumber");
            var personal = document.querySelector("#website_personalPhoneNumber");
            window.intlTelInput(office, {
                autoHideDialCode: false,
                nationalMode: false,
                placeholderNumberType: "MOBILE",
                initialCountry : 'CM'
            });
            window.intlTelInput(personal, {
                autoHideDialCode: false,
                nationalMode: false,
                placeholderNumberType: "MOBILE",
                initialCountry : 'CM'
            });
        });
    }

    var summernote = document.querySelector('.summernote');
    if (summernote) {
        head.load([
            '/admin/assets/plugins/summernote/summernote.js',
        ], function () {
            $('.summernote').summernote();
        });
    }
});